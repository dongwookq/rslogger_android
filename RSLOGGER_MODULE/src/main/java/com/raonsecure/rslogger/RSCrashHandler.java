package com.raonsecure.rslogger;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class RSCrashHandler {
    private static final String RS_HANDLER_PACKAGE_NAME = "com.raonsecure.rslogger";
    private static final String DEFAULT_HANDLER_PACKAGE_NAME = "com.android.internal.os";
    private static final String SHARED_PREFERENCES_FILE = "rsch_preferences";
    private static final String SHARED_PREFERENCES_FIELD_TIMESTAMP = "last_crash_timestamp";
    private static final String SHARED_PREFERENCES_FIELD_CRASHDATA = "last_crash_data";

    private static final int MAX_STACK_TRACE_SIZE = 131071; //128 KB - 1
    private static boolean isRSCHandler = false;

    private final static String TAG = "RSCrashHandler";
    private static Application application;


    //TODO: crash report string make function -> RSLoggerManager

    RSCrashHandler(Builder builder) {
        isRSCHandler = builder.isRSCHandler;
        setRSCrashHandler(builder.context);
    }

    private static void setRSCrashHandler(final Context context) {
        try {
            if (context != null) {
                final Thread.UncaughtExceptionHandler exceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
                if (exceptionHandler != null && exceptionHandler.getClass().getName().startsWith(RS_HANDLER_PACKAGE_NAME)) {
                    Log.e(TAG, "RSHandler was already installed, doing nothing!");
                } else {
                    if (exceptionHandler != null && !exceptionHandler.getClass().getName().startsWith(DEFAULT_HANDLER_PACKAGE_NAME)) {
                        Log.e(TAG, "You already have an UncaughtExceptionHandler. If you use a custom UncaughtExceptionHandler, it should be initialized after RSCrashHandler! Installing anyway, but your original handler will not be called.");
                    }
                }
                application = (Application) context.getApplicationContext();

                //Setup RSCrash Handler
                Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                    @Override
                    public void uncaughtException(Thread thread, Throwable throwable) {
                        Log.e(TAG, "App crashed, executing RSCrash Handler's UncaughtExceptionHandler", throwable);

                        if (hasCrashedinTheLastSeconds(application)) {
                            Log.e(TAG, "App already crashed recently, not starting custom error activity because we could enter a restart loop.", throwable);
                            if (exceptionHandler != null) {
                                exceptionHandler.uncaughtException(thread, throwable);
                                return;
                            }
                        } else {
                            setLastCrashTimestamp(application, new Date().getTime());

                            //stackTraceString
                            StringWriter stringWriter = new StringWriter();
                            PrintWriter printWriter = new PrintWriter(stringWriter);
                            throwable.printStackTrace(printWriter);
                            String stackTraceString = stringWriter.toString();
                            if (stackTraceString.length() > MAX_STACK_TRACE_SIZE) {
                                String discalaimer = " [stack trace too large]";
                                stackTraceString = stackTraceString.substring(0, MAX_STACK_TRACE_SIZE - discalaimer.length()) + discalaimer;
                            }

                            setLastCrashData(context, getAllErrorDataDetailsFormCrash(context, stackTraceString));
                            //killCurrentProcess();
                        }
                    }
                });
            } else {
                Log.e(TAG, "Context can not be null");
            }
        } catch (Throwable throwable) {
            Log.e(TAG, "RSCrashHandler can not be initialized.", throwable);
        }
    }


    private static boolean hasCrashedinTheLastSeconds (Context context) {
        long lastTimestamp = getLastCrashTimestamp(context);
        long currentTimestamp = new Date().getTime();
        return (lastTimestamp <= currentTimestamp && currentTimestamp - lastTimestamp < 3000);
    }

    private static void setLastCrashTimestamp(Context context, long timestamp) {
        context.getSharedPreferences(SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE).edit().putLong(SHARED_PREFERENCES_FIELD_TIMESTAMP, timestamp).commit();
    }

    private static long getLastCrashTimestamp (Context context) {
        return context.getSharedPreferences(SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE).getLong(SHARED_PREFERENCES_FIELD_TIMESTAMP, -1);
    }


    private static void setLastCrashData(Context context, String carshData) {
        context.getSharedPreferences(SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE).edit().putString(SHARED_PREFERENCES_FIELD_CRASHDATA, carshData).commit();
    }

    private static String getLastCrashData (Context context) {
        return context.getSharedPreferences(SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE).getString(SHARED_PREFERENCES_FIELD_CRASHDATA, "");
    }


    private static void killCurrentProcess() {
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(10);
    }

    static void closeApplication(Activity activity) {
        activity.finish();
        killCurrentProcess();
    }

    private static String getVersionName(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionName;
        } catch (Exception e) {
            return "Unknown";
        }
    }

    private static String getFirstInstallTimeAsString(Context context, DateFormat dateFormat) {
        long firstInstallTime;
        try {
            firstInstallTime = context
                    .getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0)
                    .firstInstallTime;
            return dateFormat.format(new Date(firstInstallTime));
        } catch (PackageManager.NameNotFoundException e) {
            return "";
        }
    }

    private static String getLastUpdateTimeAsString(Context context, DateFormat dateFormat) {
        long lastUpdateTime;
        try {
            lastUpdateTime = context
                    .getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0)
                    .lastUpdateTime;
            return dateFormat.format(new Date(lastUpdateTime));
        } catch (PackageManager.NameNotFoundException e) {
            return "";
        }
    }

    private static String getAllErrorDataDetailsFormCrash(Context context, String stackTraceString) {
        String LINE_SEPARATOR = "\n";
        StringBuilder errorReport = new StringBuilder();
        errorReport.append("***** RSCrash HANDLER Library ");
        errorReport.append("\n***** by RaonSecure \n");
        errorReport.append("\n***** DEVICE INFO \n");
        errorReport.append("Brand: ");
        errorReport.append(Build.BRAND);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Device: ");
        errorReport.append(Build.DEVICE);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Model: ");
        errorReport.append(Build.MODEL);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Manufacturer: ");
        errorReport.append(Build.MANUFACTURER);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Product: ");
        errorReport.append(Build.PRODUCT);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("SDK: ");
        errorReport.append(Build.VERSION.SDK);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Release: ");
        errorReport.append(Build.VERSION.RELEASE);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("\n***** APP INFO \n");
        String versionName = getVersionName(context);
        errorReport.append("Version: ");
        errorReport.append(versionName);
        errorReport.append(LINE_SEPARATOR);
        Date currentDate = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        String firstInstallTime = getFirstInstallTimeAsString(context, dateFormat);
        if (!TextUtils.isEmpty(firstInstallTime)) {
            errorReport.append("Installed On: ");
            errorReport.append(firstInstallTime);
            errorReport.append(LINE_SEPARATOR);
        }
        String lastUpdateTime = getLastUpdateTimeAsString(context, dateFormat);
        if (!TextUtils.isEmpty(lastUpdateTime)) {
            errorReport.append("Updated On: ");
            errorReport.append(lastUpdateTime);
            errorReport.append(LINE_SEPARATOR);
        }
        errorReport.append("Current Date: ");
        errorReport.append(dateFormat.format(currentDate));
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("\n***** ERROR LOG \n");
        errorReport.append(stackTraceString);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("\n***** END OF LOG *****\n");

        return errorReport.toString();
    }

    public static class Builder {
        private Context context;
        private boolean isRSCHandler = true;

        public Builder(Context context) {
            this.context = context;
        }

        public Builder setRSCHandler (boolean isRSCHandler) {
            this.isRSCHandler = isRSCHandler;
            return this;
        }

        public RSCrashHandler build() {
            return new RSCrashHandler(this);
        }
    }
}
