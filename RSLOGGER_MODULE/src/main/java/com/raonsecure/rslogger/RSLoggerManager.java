package com.raonsecure.rslogger;

import android.content.Context;

import org.apache.log4j.Logger;

public class RSLoggerManager {
    private static final Logger logger = Logger.getLogger("RSLog");
    private static final String SHARED_PREFERENCES_FILE = "rsch_preferences";
    private static final String SHARED_PREFERENCES_FIELD_CRASHDATA = "last_crash_data";

    private boolean useCrashReport = false;
    private boolean useUIEventLogging = false;

    private RSLoggerManager() { }

    private static class RSLoggerManagerHolder {
        public static final RSLoggerManager INSTANCE = new RSLoggerManager();
    }

    public static RSLoggerManager getInstance() {
        return RSLoggerManagerHolder.INSTANCE;
    }

    public void v(String tag ,String msg) {
        String tagMsg = String.format("%s: %s", tag, msg);
        logger.debug(tagMsg);
    }

    public void d(String tag ,String msg) {
        String tagMsg = String.format("%s: %s", tag, msg);
        logger.debug(tagMsg);
    }

    public void i(String tag ,String msg) {
        String tagMsg = String.format("%s: %s", tag, msg);
        logger.info(tagMsg);
    }

    public void w(String tag ,String msg) {
        String tagMsg = String.format("%s: %s", tag, msg);
        logger.warn(tagMsg);
    }

    public void e(String tag ,String msg) {
        String tagMsg = String.format("%s: %s", tag, msg);
        logger.error(tagMsg);
    }

    public boolean isUseCrashReport() {
        return useCrashReport;
    }

    public void setUseCrashReport(final boolean useCrashReport, Context context) {
        this.useCrashReport = useCrashReport;
        new RSCrashHandler.Builder(context).setRSCHandler(useCrashReport).build();
    }

    public boolean hasLastCrashed(Context context) {
        String crashReport = context.getSharedPreferences(SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE).getString(SHARED_PREFERENCES_FIELD_CRASHDATA, "");
        return (crashReport.length() > 0);
    }

    public String getLastCrashReportAndFlush(Context context) {
        String lastCrashReport = context.getSharedPreferences(SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE).getString(SHARED_PREFERENCES_FIELD_CRASHDATA, "");
        context.getSharedPreferences(SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE).edit().putString(SHARED_PREFERENCES_FIELD_CRASHDATA, "").commit();
        return lastCrashReport;
    }


    public boolean isUseUIEventLogging() {
        return useUIEventLogging;
    }

    public void setUseUIEventLogging(final boolean useUIEventLogging) {
        this.useUIEventLogging = useUIEventLogging;
    }
}
