package com.raonsecure.rslogger;

import android.content.Context;
import android.util.Log;

import java.io.File;

public class RSLogger {
    private static final String TAG = RSLogger.class.getSimpleName();
    private static RSLoggerConfigurator configurator = new RSLoggerConfigurator();
    private static RSLoggerManager loggerManager;

    public enum RSLogLevel {
        RSLogLevelVerbose,
        RSLogLevelDebug,
        RSLogLevelInfo,
        RSLogLevelWarning,
        RSLogLevelError;

        private RSLogLevel() {
        }
    }

    public enum RSLoggerType {
        RSGenericLogger,
        RSForceLogger;

        private RSLoggerType() {
        }
    }

    public RSLogger() {
    }

    //TODO: builder pattern

    public static synchronized void init(Context context) {
        String applicationName = context.getApplicationInfo().labelRes == 0 ? context.getApplicationInfo().nonLocalizedLabel.toString() : context.getString(context.getApplicationInfo().labelRes);
        String fileName = context.getFilesDir() + "/LOG_FILE/" + applicationName + ".log";
        String fileNameForce = context.getFilesDir() + "/LOG_FILE/" + applicationName + "_Admin.log";

        if (context != null) {
            if (loggerManager == null) {
                try {
                    configurator.setRsContext(context);
                    configurator.setFileName(fileName);
                    configurator.setFileNameForForce(fileNameForce);
                    configurator.setFilePattern(configurator.getFilePattern());
                    configurator.setMaxBackupSize(configurator.getMaxBackupSize());
                    configurator.setMaxFileSize(configurator.getMaxFileSize(RSLoggerType.RSGenericLogger), RSLoggerType.RSGenericLogger);
                    configurator.setMaxFileSize(configurator.getMaxFileSize(RSLoggerType.RSForceLogger), RSLoggerType.RSForceLogger);
                    loggerManager = RSLoggerManager.getInstance();

                } catch (SecurityException exception) {
                }
            }
        } else {
            Log.w(TAG, "WARNING: The RSLogger sdk is not initialized. The context provided is null.");
        }
    }


    public static void configure() {
        configurator.configure();
    }

    public static void setLogLevel(final RSLogLevel level) {
        configurator.setLevel(level);
    }

    public static RSLogLevel getLogLevel() {
        return configurator.getLevel();
    }

    public static String getFileName() {
        return configurator.getFileName();
    }

    public static long getMaxFileSize(final RSLoggerType type) {
        return configurator.getMaxFileSize(type);
    }
    public static void setMaxFileSize(final long maxFileSize, final RSLoggerType type) {
        configurator.setMaxFileSize(maxFileSize, type);
    }

    public boolean isImmediateFlush() {
        return configurator.isImmediateFlush();
    }
    public void setImmediateFlush(final boolean immediateFlush) {
        configurator.setImmediateFlush(immediateFlush);
    }

    public static int getMaxPeriodDay() {
        return configurator.getMaxPeriodDay();
    }

    public static void setMaxPeriodDay(int maxPeriodDay) {
        configurator.setMaxPeriodDay(maxPeriodDay);
    }

    public static void setUIEventLogging(boolean enabled) {
        loggerManager.setUseUIEventLogging(enabled);
    }

    public static void setCrashReport(boolean enabled) {
        loggerManager.setUseCrashReport(enabled, configurator.getRsContext());
    }

    public static boolean hasLastCrashed() {
        return loggerManager.hasLastCrashed(configurator.getRsContext());
    }

    public static String getLastCrashReportAndFlush() {
        return loggerManager.getLastCrashReportAndFlush(configurator.getRsContext());
    }

    public static File getLogFile(RSLoggerType type) {
        if (RSLoggerType.RSGenericLogger == type) {
            File logFile = new File(configurator.getFileName());
            return logFile;
        } else if (RSLoggerType.RSForceLogger == type) {
            File logFile = new File(configurator.getFileNameForForce());
            return logFile;
        } else {
            return null;
        }
    }

    public static void e(final String tag, final String msg) {
        if (isRSLoggerInitialized()) {
            loggerManager.e(tag, msg);
        }
    }

    public static void w(final String tag, final String msg) {
        if (isRSLoggerInitialized()) {
            loggerManager.w(tag, msg);
        }
    }

    public static void i(final String tag, final String msg) {
        if (isRSLoggerInitialized()) {
            loggerManager.i(tag, msg);
        }
    }

    public static void d(final String tag, final String msg) {
        if (isRSLoggerInitialized()) {
            loggerManager.d(tag, msg);
        }
    }

    public static void v(final String tag, final String msg) {
        if (isRSLoggerInitialized()) {
            loggerManager.v(tag, msg);
        }
    }

    private static boolean isRSLoggerInitialized() {
        if (loggerManager == null) {
            Log.w(TAG, "WARNING: RSLogger SDK is not initialized. You should call first to the method RSLogger.init()");
            return false;
        } else {
            return true;
        }
    }
}
