package com.raonsecure.rslogger;

import android.content.Context;
import android.view.View;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class RSAspect {
    private static final String TAG = "RSL";

    @Pointcut("execution(* android.view.View.OnClickListener.*(..))")
    public void onClickPoincut(){}

    @Pointcut("execution(* android.app.Activity.onCreate(..))")
    public void onCreateCutPoint() {}

    @Pointcut("execution(* android.app.Activity.onResume(..))")
    public void onResumeCutPoint() {}

    @Pointcut("execution(* android.app.Activity.onPause(..))")
    public void onPauseCutPoint() {}

    @Pointcut("execution(* android.app.Activity.onStart(..))")
    public void onStartCutPoint() {}

    @Pointcut("execution(* android.app.Activity.onStop(..))")
    public void onStopCutPoint() {}

    @Pointcut("execution(* android.app.Activity.onDestroy(..))")
    public void onDestroyCutPoint() {}


    //TODO: uievent internal log function

    @After("onClickPoincut()")
    public void atferOnClick(JoinPoint joinPoint){
        if (!RSLoggerManager.getInstance().isUseUIEventLogging()) {
            return;
        }

        Object[] args = joinPoint.getArgs();
        String entryName = "";
        if (args.length > 0) {
            View view = (View) args[0];
            Context context = view.getContext();
            int viewResId = view.getId();
            if (context != null && viewResId > 0) {
                entryName = view.getContext().getResources().getResourceEntryName(view.getId());
                RSLoggerManager.getInstance().i(TAG, "[Interaction] Action [" + view.getContext() + "] by sender view (currentTitle:\"" + entryName + "\")");
            } else {
                RSLoggerManager.getInstance().i(TAG, "[Interaction] Action currentTitle:\"" + joinPoint.getTarget() + "\"");
            }
        } else {
            RSLoggerManager.getInstance().i(TAG, "[Interaction] Action currentTitle:\"" + joinPoint.getTarget() + "\"");
        }

    }

    @After("onCreateCutPoint()")
    public void atferOnCreate(JoinPoint joinPoint){
        if (!RSLoggerManager.getInstance().isUseUIEventLogging()) {
            return;
        }

        RSLoggerManager.getInstance().i(TAG, "[UI] Front-most activity onCreate:" + joinPoint.getTarget());
    }

    @After("onResumeCutPoint()")
    public void atferOnResume(JoinPoint joinPoint){
        if (!RSLoggerManager.getInstance().isUseUIEventLogging()) {
            return;
        }
        RSLoggerManager.getInstance().i(TAG, "[UI] Front-most activity onResume:" + joinPoint.getTarget());
    }

    @After("onPauseCutPoint()")
    public void atferOnPause(JoinPoint joinPoint){
        if (!RSLoggerManager.getInstance().isUseUIEventLogging()) {
            return;
        }

        RSLoggerManager.getInstance().i(TAG, "[UI] Front-most activity onPause:" + joinPoint.getSignature());
    }

    @After("onStartCutPoint()")
    public void atferOnStart(JoinPoint joinPoint){
        if (!RSLoggerManager.getInstance().isUseUIEventLogging()) {
            return;
        }

        RSLoggerManager.getInstance().i(TAG, "[UI] Front-most activity onStart:" + joinPoint.getSignature());
    }

    @After("onStopCutPoint()")
    public void atferOnStop(JoinPoint joinPoint){
        if (!RSLoggerManager.getInstance().isUseUIEventLogging()) {
            return;
        }

        RSLoggerManager.getInstance().i(TAG, "[UI] Front-most activity onStop:" + joinPoint.getSignature());
    }

    @After("onDestroyCutPoint()")
    public void atferOnDestroy(JoinPoint joinPoint){
        if (!RSLoggerManager.getInstance().isUseUIEventLogging()) {
            return;
        }

        RSLoggerManager.getInstance().i(TAG, "[UI] Front-most activity onDestory:" + joinPoint.getSignature());
    }
}
